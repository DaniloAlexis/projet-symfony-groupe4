<?php

namespace App\Entity;

use App\Repository\SortieRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SortieRepository::class)
 */
class Sortie
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateheuredebut;

    /**
     * @ORM\Column(type="integer")
     */
    private $duree;

    /**
     * @ORM\Column(type="datetime")
     */
    private $datelimiteinscription;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $nbinscriptionsmax;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $infossortie;

    /**
     * @var Lieu
     * @ORM\ManyToOne(targetEntity="App\Entity\Lieu", inversedBy="sorties")
     */
    private $lieu;

    /**
     * @var Etat
     * @ORM\ManyToOne(targetEntity="App\Entity\Etat", inversedBy="sorties")
     */
    private $etat;

    /**
     * @var Site
     * @ORM\ManyToOne(targetEntity="App\Entity\Site", inversedBy="sorties")
     */
    private $site;

    /**
     * @var Organisateur
     * @ORM\ManyToOne(targetEntity="App\Entity\Participant", inversedBy="sorties")
     */
    private $organisateur;

    /**
     * @var Participants
     * @ORM\ManyToMany(targetEntity="App\Entity\Participant", inversedBy="inscription_sorties")
     */
    private $participants;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDateheuredebut(): ?\DateTimeInterface
    {
        return $this->dateheuredebut;
    }

    public function setDateheuredebut(\DateTimeInterface $dateheuredebut): self
    {
        $this->dateheuredebut = $dateheuredebut;

        return $this;
    }

    public function getDuree(): ?int
    {
        return $this->duree;
    }

    public function setDur�e(int $duree): self
    {
        $this->duree = $duree;

        return $this;
    }

    public function getDatelimiteinscription(): ?\DateTimeInterface
    {
        return $this->datelimiteinscription;
    }

    public function setDatelimiteinscription(\DateTimeInterface $datelimiteinscription): self
    {
        $this->datelimiteinscription = $datelimiteinscription;

        return $this;
    }

    public function getNbinscriptionsmax(): ?int
    {
        return $this->nbinscriptionsmax;
    }

    public function setNbinscriptionsmax(?int $nbinscriptionsmax): self
    {
        $this->nbinscriptionsmax = $nbinscriptionsmax;

        return $this;
    }

    public function getInfossortie(): ?string
    {
        return $this->infossortie;
    }

    public function setInfossortie(?string $infossortie): self
    {
        $this->infossortie = $infossortie;

        return $this;
    }


}
